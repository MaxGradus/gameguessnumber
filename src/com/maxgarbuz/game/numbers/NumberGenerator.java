package com.maxgarbuz.game.numbers;

import java.util.Random;

/**
 * Класс, генерирующий случайные числа
 */

public class NumberGenerator {
    private static final int MAX_LEVEL = 3;

    public int generateNumber(int level) {
        Random random = new Random();
        switch (level) {
            case 1:
                return random.nextInt(10);
            case 2:
                return random.nextInt(100);
            case 3:
                return random.nextInt(1000);
            default:
                throw new IllegalArgumentException("level = " + level + " is not supported");
        }
    }

    public int getMaxNumber(int level) {
        return (int) (Math.pow(10, level) - 1);
    }

    public boolean checkNumberInRange(int level, int num) {
        return 0 <= num && num <= getMaxNumber(level);
    }

    public int getNextLevel(int level) {
        if (MAX_LEVEL == level) {
            return 1;
        } else {
            return level+1;
        }
    }
}

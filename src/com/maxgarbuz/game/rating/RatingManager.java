package com.maxgarbuz.game.rating;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Класс для загрузки и записи рейтинга
 */

public class RatingManager {
    private static final String RATINGS_FILE = "ratings.txt";
    private List<Rating> ratings;

    {
        try {
            loadRatings();
        } catch (IOException e) {
            System.err.println("Error while reading ratings file: " + e.getMessage());
        }
    }


    private void loadRatings() throws IOException {
        ratings = new ArrayList<>();
        File ratingsFile = new File(RATINGS_FILE);
        if (ratingsFile.exists()) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(ratingsFile)))) {
                while (reader.ready()) {
                    String ratingLine = reader.readLine();
                    Rating rating = Rating.parseFromString(ratingLine);
                    ratings.add(rating);
                }
            }
        } else {
            // фаил не найден, значит список рейтинга остается пустым
        }
    }

    private void writeRatings() throws IOException {
        File ratingsFile = new File(RATINGS_FILE);
        if (ratingsFile.exists() || ratingsFile.createNewFile()) {
            try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(ratingsFile)))) {
                for (Rating rating : ratings) {
                    writer.write(Rating.parseToString(rating));
                    writer.newLine();
                }
            }
        } else {
            System.err.println("File " + RATINGS_FILE + " cannot be created");
        }
    }

    public List<Rating> getRatings(int level) {
        List<Rating> ratingsByLevel = new ArrayList<>();
        for (Rating rating : ratings) {
            if (level == rating.getLevel()) {
                ratingsByLevel.add(rating);
            }
        }
        return orderRatings(ratingsByLevel);
    }

    public void addRating(Rating rating) {
        ratings.add(rating);
        try {
            writeRatings();
        } catch (IOException e) {
            System.err.println("Error while writing ratings file: " + e.getMessage());
        }
    }

    private static List<Rating> orderRatings(List<Rating> ratings) {
        Collections.sort(ratings, new Comparator<Rating>() {
            @Override
            public int compare(Rating rr, Rating rl) {
                return Integer.compare(rr.getCountOfAttempts(), rl.getCountOfAttempts());
            }
        });
        return ratings;
    }
}


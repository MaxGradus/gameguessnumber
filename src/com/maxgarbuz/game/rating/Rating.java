package com.maxgarbuz.game.rating;

import java.io.Serializable;

/**
 * Класс, представляющий собой одну запись в рейтинге
 */

public class Rating implements Serializable {
    private String firstName;
    private String lastName;
    private int level;
    private int countOfAttempts;

    public Rating() {}

    public Rating(String firstName, String lastName, int level, int countOfAttempts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.level = level;
        this.countOfAttempts = countOfAttempts;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getCountOfAttempts() {
        return countOfAttempts;
    }

    public void setCountOfAttempts(int countOfAttempts) {
        this.countOfAttempts = countOfAttempts;
    }


    public static Rating parseFromString(String ratingStr) {
        String[] elements = ratingStr.split(",");
        if (elements.length < 4) throw new IllegalArgumentException("Rating string = " + ratingStr + " has incorrect format");

        Rating rating = new Rating();
        rating.setFirstName(elements[0]);
        rating.setLastName(elements[1]);
        rating.setLevel(Integer.valueOf(elements[2]));
        rating.setCountOfAttempts(Integer.valueOf(elements[3]));
        return rating;
    }


    public static String parseToString(Rating rating) {
        return rating.getFirstName() + "," + rating.getLastName() + "," + rating.getLevel() + "," + rating.getCountOfAttempts();
    }
}

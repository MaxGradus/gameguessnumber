package com.maxgarbuz.game;

import com.maxgarbuz.game.numbers.NumberGenerator;
import com.maxgarbuz.game.rating.Rating;
import com.maxgarbuz.game.rating.RatingManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class GuessNumber {
    private static BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
    private static NumberGenerator numberGenerator = new NumberGenerator();
    private static RatingManager ratingManager = new RatingManager();

    public static void main(String[] args) throws IOException {
        printWelcome();

        int level = 1;
        boolean exit = false;
        while (!exit) {
            System.out.println("Уровень сложности = " + level + ", " +
                    "загаданное число лежит в диапазоне [0, " + (int) (Math.pow(10, level) - 1) + "]");
            int guessNum = numberGenerator.generateNumber(level);
            int userNum;
            int countOfAttempts = 0;
            while (true) {
                System.out.print("Введите число >> ");
                String answerStr = console.readLine();
                try {
                    userNum = Integer.parseInt(answerStr);
                    if (!numberGenerator.checkNumberInRange(level, userNum)) {
                        throw new IllegalArgumentException("Number is out of range");
                    }

                    countOfAttempts++;
                    if (guessNum == userNum) {
                        System.out.println("УРА! Вы угадали число с " + countOfAttempts + " попыток, это число = " + guessNum);
                        break;
                    } else {
                        String direction = (guessNum > userNum) ? "больше" : "меньше";
                        System.out.print("Загаданное число " + direction + " введенного, попробуйте еще раз.\n");
                    }
                } catch (IllegalArgumentException e) {
                    System.out.println("Некорректное число! Пожалуйста, повторите ввод");
                }
            }

            updateRating(level, countOfAttempts);

            if (doContinue()) {
                level = numberGenerator.getNextLevel(level);
            } else {
                exit = true;
            }
        }
        System.out.println("Спасибо за игру!");
        console.close();
    }

    private static void printWelcome() {
        System.out.println("Добро пожаловать в игру Угадай число!\n" +
                "Правила: Компьютер загадывает число и Вам нужно его угадать с нименьшим количеством попыток.\n");
    }

    private static boolean doContinue() throws IOException {
        System.out.println("\nХотите продолжить игру? д/н");
        String answer = console.readLine();
        return "д".equals(answer);
    }

    private static void updateRating(int level, int countOfAttempts) throws IOException {
        System.out.print("\nВаше имя >> ");
        String firstName = console.readLine();
        System.out.print("Ваша фамилия >> ");
        String lastName = console.readLine();
        ratingManager.addRating(new Rating(firstName, lastName, level, countOfAttempts));

        System.out.println("Уровень " + level + " Рейтинг игроков:");
        int position = 0;
        for (Rating rating : ratingManager.getRatings(level)) {
            position = position + 1;
            System.out.println(position + " (" + rating.getCountOfAttempts() + " попыток)" +
                    " - " + rating.getFirstName() + " " + rating.getLastName());
        }
    }
}


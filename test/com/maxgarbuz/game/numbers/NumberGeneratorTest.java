package com.maxgarbuz.game.numbers;


import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class NumberGeneratorTest {

    @Test
    public void testGenerateNumber() {
        // проверяем, что генерируемые числа лежат в корректном диапазоне
        NumberGenerator generator = new NumberGenerator();
        for (int i = 0; i < 10; i++) {
            int num_1 = generator.generateNumber(1);
            int num_2 = generator.generateNumber(2);
            int num_3 = generator.generateNumber(3);
            assertTrue(0 <= num_1 && num_1 <= 9);
            assertTrue(0 <= num_2 && num_2 <= 99);
            assertTrue(0 <= num_3 && num_3 <= 999);
        }
    }

    @Test
    public void testGetMaxNumber() {
        // проверяем максимальные границы диапазонов
        NumberGenerator generator = new NumberGenerator();
        assertEquals(9, generator.getMaxNumber(1));
        assertEquals(99, generator.getMaxNumber(2));
        assertEquals(999, generator.getMaxNumber(3));
    }

    @Test
    public void testCheckNumberInRange() {
        // проверяем логику принадлежности числа к диапазону
        NumberGenerator generator = new NumberGenerator();
        assertTrue(generator.checkNumberInRange(1, 0));
        assertTrue(generator.checkNumberInRange(1, 5));
        assertFalse(generator.checkNumberInRange(1, 10));
        assertFalse(generator.checkNumberInRange(1, 29));

        assertTrue(generator.checkNumberInRange(2, 0));
        assertTrue(generator.checkNumberInRange(2, 50));
        assertFalse(generator.checkNumberInRange(2, 100));
        assertFalse(generator.checkNumberInRange(2, 182));

        assertTrue(generator.checkNumberInRange(3, 0));
        assertTrue(generator.checkNumberInRange(3, 532));
        assertFalse(generator.checkNumberInRange(3, 1000));
        assertFalse(generator.checkNumberInRange(3, 2801));
    }
}

